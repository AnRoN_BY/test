import './tableWithFilter.css';

function TableWithFilter(props) {

  const columns = props.posts[0] && Object.keys(props.posts[0]);

  return (
      <div className={'table'}>
        <table>
          <thead>
            <tr>{props.posts[0] && columns.map((item, index) => <th id={index} key={index}>{item}</th>)}</tr>
          </thead>
          <tbody>
            {props.posts.map((row, index) =>
              <tr key={index}>
                {columns.map(column => <td key={column.id}>{row[column]}</td>)}
              </tr>
            )}
          </tbody>
        </table>
      </div>
  );
}

export default TableWithFilter;
