import React, {useState, useEffect} from 'react';
import './parentComponent.css';
import _ from 'lodash';
import TableWithFilter from '../Table/TableWithFilter';
import Filter from '../Filter/Filter';
import Pagination from '../Pagination/Pagination';

function ParentComponent() {

  const [posts, setPosts] = useState([]);
  const [filterValue, setFilterValue] = useState('');
  const [searchColumnsValue, setSearchColumnsValue] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);

  const value = (filter) => {
      setFilterValue(filter)
  }

  const searchValue = (searchColumns) => {
      setSearchColumnsValue(searchColumns)
  }

  const pageValue = (pages) => {
      setPageSize(pages)
  }

  const countPages = posts ? Math.ceil(posts.length/pageSize) : 0;
  const pages = _.range(1, countPages+1);

  useEffect(() => {
      fetch('https://jsonplaceholder.typicode.com/posts')
          .then(response => response.json())
          .then(json => {setPosts(json)})
  }, []);

  function search(rows) {

     return rows.filter((row) =>
         searchColumnsValue.some(
            (column) => row[column].toString().toLowerCase().startsWith(filterValue)
         )
     )
  }

  const columns = posts[0] && Object.keys(posts[0]);
  const lastPostIndex = currentPage * pageSize;
  const firstPostIndex = lastPostIndex - pageSize;
  const currentPosts = posts.slice(firstPostIndex, lastPostIndex);

  const paginate = (page) => {
      setCurrentPage(page)
  }

  return (
    <div className={'parent_component'}>
      <Filter
          columns={columns}
          searchColumnsValue={searchColumnsValue}
          setToParent={value}
          setToParentSearch={searchValue}
      />
      <TableWithFilter
          posts={filterValue === '' ? currentPosts : search(currentPosts)}
      />
      <Pagination
          paginate={paginate}
          pages={pages}
          posts={posts}
          setToParentPages={pageValue}
      />
    </div>
  );
}

export default ParentComponent;
