import './pagination.css';
import React, {useState} from 'react';

function Pagination(props) {

  const [pageValue, setPageValue] = useState('');

  return (
      <div className={'pagination_component'}>
          <div>
              Change pagesCount:
              <input
                  className={'pages_number'}
                  type={'number'}
                  value={pageValue < 0 ? 1 : pageValue}
                  onChange={(e) => {
                      setPageValue(e.target.value);
                  }}
                  min={1}
              />
              <input
                  className={'submit_pages_number'}
                  type={'submit'}
                  value={'Send'}
                  onClick={() => {props.setToParentPages(pageValue)}}
              />
          </div>
          <nav className={'pagination'}>
              <ul>
                  {props.pages.map((page) => (<li onClick={() => props.paginate(page)}>{page}</li>))}
              </ul>
          </nav>
      </div>
  );
}

export default Pagination;
