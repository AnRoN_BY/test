import './filter.css';
import React, {useState} from 'react';

function Filter(props) {

  const [filter, setFilter] = useState('');
  const [searchColumns, setSearchColumns] = useState([]);

  return (
    <div>
      <div>
          {props.columns && props.columns.map((column, index) => (
              <label>
                  <input
                      key={index}
                      onClick={props.setToParentSearch(searchColumns)}
                      type={'checkbox'}
                      checked={props.searchColumnsValue.includes(column)}
                      onChange={(e) => {
                          const checked = props.searchColumnsValue.includes(column);
                          setSearchColumns(prev => checked ? prev.filter(sc => sc !== column)
                          : [...prev, column])
                      }}
                  />
                  {column}
              </label>
          ))}
      </div>
      <input
          className={'search'}
          type={'text'}
          value={filter}
          placeholder={'Search'}
          onChange={
              (e) => {
                  setFilter(e.target.value);
                  props.setToParent(e.target.value);
              }
          }
      />
      </div>

  );
}

export default Filter;
